# Node Task

This project contains node-task

### JSDOC CODE

```js
/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => {
    return a + b;
}
```
Easy way to document functions in javascript
```javascript
/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const dec = (a,b) => {
  return a - b;
}

app.get('/dec/', (req, res) => {
  const x = dec(1, 2)
  res.send(`Decrement: ${x}`);
})
```
The way I used a new function to decrement numbers