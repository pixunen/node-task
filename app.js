const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a,b) => {
    return a + b;
}
/**
 * 
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const dec = (a,b) => {
  return a - b;
}

app.get('/dec/', (req, res) => {
  const x = dec(1, 2)
  res.send(`Decrement: ${x}`);
})

app.get('/add/', (req, res) => {
   const x = add(1, 2)
   res.send(`Sum: ${x}`);
})

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`listening at http://localhost:${port}`)
})